![](https://gitlab.com/GRIDARK/vasilii-grigorev.com/badges/main/pipeline.svg "pipeline status") ![](https://gitlab.com/GRIDARK/vasilii-grigorev.com/-/badges/release.svg "last release") 

# vasilii-grigorev.com

Персональный сайт-визитка, описывающий меня-любимого как профессионального разработчика

# Дизайн

Сайт основан на шаблоне https://www.designstub.com/product/defolio-bootstrap-5-html-resume-template/

# Сборка и разработка

`npm run start` - запуск локального сервера с функцией livereload и сборкой проекта с отслеживанием файлов

`npm run build` - сборка проекта в папку `dist`

`npm run favicons` - генерация favicons

# Зависимости

Проект собирается с помощью Node.js 20.15.0 и npm 10.7.0