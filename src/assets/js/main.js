document.addEventListener(
    "DOMContentLoaded",
    function () {
        /**
         * Get absolute height
         * @param {HTMLElement} element
         * @returns {Number}
         */
        function getAbsoluteHeight(element) {
            var styles = window.getComputedStyle(element);
            var margin = parseFloat(styles["marginTop"]) + parseFloat(styles["marginBottom"]);

            return Math.ceil(element.offsetHeight + margin);
        }

        let mastheadheight = getAbsoluteHeight(document.getElementsByClassName("ds-header")[0]);
        document.querySelector(".ds-banner,.ds-main-section").style.marginTop = mastheadheight + "px";

        window.addEventListener("scroll", function () {
            if (document.documentElement.scrollTop >= 10) {
                document.getElementsByClassName("ds-header")[0].classList.add("ds-fixed-header");
            } else {
                document.getElementsByClassName("ds-header")[0].classList.remove("ds-fixed-header");
            }
        });

        window.dispatchEvent(new Event("scroll"));
    },
    false
);
